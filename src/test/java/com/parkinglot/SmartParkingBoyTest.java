package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SmartParkingBoyTest {

    @Test
    public void should_return_ticket_when_parking_car_given_a_car_and_smartParkingBoy_with_multiply_parkingLot() {
        Car car = new Car();

        SmartParkingBoy parkingBoy = new SmartParkingBoy(new ParkingLot(1));
        parkingBoy.addParkingLot(new ParkingLot(1));

        Ticket ticket = parkingBoy.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    public void should_return_car_when_fetching_car_given_ticket_and_smartParkingBoy_with_multiply_parkingLot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLot);


        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    public void should_park_car_into_second_empty_parkingLot_when_parking_car_given_smartParkingBoy_with_multiply_parkingLot() {
        Car car1 = new Car();
        Car car2 = new Car();

        SmartParkingBoy parkingBoy = new SmartParkingBoy(new ParkingLot(10));
        parkingBoy.addParkingLot(new ParkingLot(10));

        parkingBoy.park(car1);
        Ticket car2Ticket = parkingBoy.park(car2);

        ParkingLot parkingLot = parkingBoy.getParkingLot(2);
        Assertions.assertEquals(true, parkingLot.containCar(car2Ticket));

    }

    @Test
    public void should_return_message_when_fetch_car_given_wrong_ticket_and_smartParkingBoy_with_multiply_parkingLot() {
        Car car1 = new Car();
        SmartParkingBoy parkingBoy = new SmartParkingBoy(new ParkingLot(1));

        Ticket ticket = parkingBoy.park(car1);
        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_message_when_fetch_car_given_used_ticket_and_samrtParkingBoy_with_multiply_parkingLot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        SmartParkingBoy parkingBoy = new SmartParkingBoy(parkingLot);


        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_message_when_parking_car_given_a_car_and_smartParkingBoy_with_parkingLot_without_position(){
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        SmartParkingBoy parkingBoy = new SmartParkingBoy(new ParkingLot(1));
        parkingBoy.addParkingLot(new ParkingLot(1));

        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);


        UnAvailablePositionException unAvailablePositionException = Assertions.assertThrows(UnAvailablePositionException.class, () -> parkingBoy.park(car3));
        Assertions.assertEquals("no available position", unAvailablePositionException.getMessage());
    }
}
