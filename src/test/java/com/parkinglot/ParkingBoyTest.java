package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingBoyTest {

    @Test
    public void should_return_ticket_when_park_car_given_one_car_and_parkingLot_and_parkingBoy() {

        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        Ticket ticket = parkingBoy.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    public void should_return_car_when_fetch_car_given_one_ticket_and_parkingLot_parked_car_and_parkingBoy() {

        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);


        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_each_car_when_fetch_car_given_two_ticket_and_parkingLot_parked_car_and_parkingBoy() {
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);


        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);

        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }

    @Test
    public void should_return_message_when_fetch_car_given_wrong_ticket_and_parkingLot_and_parkingBoy() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        Ticket ticket = parkingBoy.park(car);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_message_when_fetch_car_given_used_ticket_and_parkingLot_and_parkingBoy() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);


        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_message_when_parking_car_given_a_car_and_parkingLot_without_position_and_parkingBoy() {

        Car car1 = new Car();
        Car car2 = new Car();
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        Ticket ticket1 = parkingBoy.park(car1);

        UnAvailablePositionException unAvailablePositionException = Assertions.assertThrows(UnAvailablePositionException.class, () -> parkingBoy.park(car2));
        Assertions.assertEquals("no available position", unAvailablePositionException.getMessage());
    }



    @Test
    public void should_return_ticket_when_parking_car_given_a_car_and_parkingBoy_with_multiply_parkingLot() {
        Car car1 = new Car();

        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(1));
        parkingBoy.addParkingLot(new ParkingLot(1));
        parkingBoy.addParkingLot(new ParkingLot(1));
        parkingBoy.addParkingLot(new ParkingLot(1));

        Ticket ticket1 = parkingBoy.park(car1);

        Assertions.assertNotNull(ticket1);
    }

    @Test
    public void should_return_car_when_fetching_car_given_ticket_parkingBoy_with_multiply_parkingLot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);


        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    public void should_park_car_into_second_parkingLot_when_parking_car_given_parkingBoy_with_multiply_parkingLot() {
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(1));
        parkingBoy.addParkingLot(new ParkingLot(1));

        parkingBoy.park(car1);
        Ticket car2Ticket = parkingBoy.park(car2);

        ParkingLot parkingLot = parkingBoy.getParkingLot(2);
        Assertions.assertNotNull(parkingLot.containCar(car2Ticket));

    }

    @Test
    public void should_return_message_when_fetch_car_given_wrong_ticket_and_and_parkingBoy_with_multiply_parkingLot() {
        Car car1 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(1));

        Ticket ticket = parkingBoy.park(car1);
        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingBoy.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_message_when_fetch_car_given_used_ticket_and_parkingBoy_with_multiply_parkingLot() {
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);


        Ticket ticket = parkingBoy.park(car);
        Car fetchedCar = parkingBoy.fetch(ticket);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_message_when_parking_car_given_a_car_and_parkingBoy_with_parkingLot_without_position(){
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(new ParkingLot(1));
        parkingBoy.addParkingLot(new ParkingLot(1));

        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);


        UnAvailablePositionException unAvailablePositionException = Assertions.assertThrows(UnAvailablePositionException.class, () -> parkingBoy.park(car3));
        Assertions.assertEquals("no available position", unAvailablePositionException.getMessage());
    }
}
