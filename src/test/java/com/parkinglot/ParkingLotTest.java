package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {


    @Test
    public void should_return_ticket_when_park_car_given_one_car_and_parkingLot(){

        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot();

        Ticket ticket = parkingLot.park(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    public void should_return_car_when_fetch_car_given_one_ticket_and_parkingLot_parked_car(){

        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        Ticket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_each_car_when_fetch_car_given_two_ticket_and_parkingLot_parked_car(){
        ParkingLot parkingLot = new ParkingLot();
        Car car1 = new Car();
        Car car2 = new Car();


        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);

        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);

        Assertions.assertEquals(car1, fetchedCar1);
        Assertions.assertEquals(car2, fetchedCar2);
    }


    @Test
    public void should_return_null_car_when_fetch_car_given_wrong_ticket_and_parkingLot(){
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        Ticket ticket = parkingLot.park(car);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLot.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_null_car_when_fetch_car_given_used_ticket_and_parkingLot(){
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        Ticket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLot.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_nothing_when_parking_car_given_a_car_and_parkingLot_without_position(){

        ParkingLot parkingLot = new ParkingLot(1);
        Car car1 = new Car();
        Car car2 = new Car();

        Ticket ticket1 = parkingLot.park(car1);

        UnAvailablePositionException unAvailablePositionException = Assertions.assertThrows(UnAvailablePositionException.class, () -> parkingLot.park(car2));
        Assertions.assertEquals("no available position", unAvailablePositionException.getMessage());
    }
}
