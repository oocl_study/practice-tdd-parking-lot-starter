package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotServiceManagerTest {

    @Test
    public void should_return_ticket_when_parking_car_by_parkingLotServiceManager_given_a_car_and_parkingLotServiceManager() {
        Car car = new Car();

        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(new ParkingLot(1));
        parkingLotManager.addParkingLot(new ParkingLot(1));

        Ticket ticket = parkingLotManager.park(car);

        Assertions.assertNotNull(ticket);

    }

    @Test
    public void should_return_car_when_fetching_car_by_parkingLotServiceManager_given_ticket_and_parkingLotServiceManager() {
        Car car = new Car();
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(new ParkingLot(1));


        Ticket ticket = parkingLotManager.park(car);
        Car fetchedCar = parkingLotManager.fetch(ticket);

        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    public void should_return_wrong_message_when_fetch_car_given_wrong_ticket_and_parkingLotManager(){
        Car car = new Car();
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(new ParkingLot(1));

        Ticket ticket = parkingLotManager.park(car);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLotManager.fetch(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_wrong_message_when_fetch_car_given_used_ticket_parkingLotManager() {
        Car car = new Car();
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(new ParkingLot());


        Ticket ticket = parkingLotManager.park(car);
        Car fetchedCar = parkingLotManager.fetch(ticket);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLotManager.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_wrong_message_when_parking_car_given_car_and_parkingLotManager_with_parkingLot_without_position(){
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(new ParkingLot(1));
        parkingLotManager.addParkingLot(new ParkingLot(1));

        Ticket ticket1 = parkingLotManager.park(car1);
        Ticket ticket2 = parkingLotManager.park(car2);


        UnAvailablePositionException unAvailablePositionException = Assertions.assertThrows(UnAvailablePositionException.class, () -> parkingLotManager.park(car3));
        Assertions.assertEquals("no available position", unAvailablePositionException.getMessage());
    }

    @Test
    public void should_return_ticket_when_parking_car_by_parkingBoy_given_a_car_and_parkingLotServiceManager(){
        Car car = new Car();

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot());
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);

        Ticket ticket = parkingLotManager.parkByParkingBoy(car);

        Assertions.assertNotNull(ticket);
    }

    @Test
    public void should_return_car_when_fetching_car_from_parkingBoy_given_ticket_and_parkingLotServiceManager(){
        Car car = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot());
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);

        Ticket ticket = parkingLotManager.parkByParkingBoy(car);
        Car fetchCar = parkingLotManager.fetchByParkingBoy(ticket);

        Assertions.assertNotNull(fetchCar);
    }

    @Test
    public void should_return_wrong_message_when_fetch_car_from_parkingBoy_given_wrong_ticket_and_parkingLotManager() {
        Car car = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot());
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);

        Ticket ticket = parkingLotManager.parkByParkingBoy(car);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLotManager.fetchByParkingBoy(new Ticket()));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_wrong_message_when_fetch_car_from_parkingBoy_given_used_ticket_and_parkingLotManager() {
        Car car = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot());
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot());
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);

        Ticket ticket = parkingLotManager.parkByParkingBoy(car);
        Car fetchedCar = parkingLotManager.fetchByParkingBoy(ticket);

        UnRecognizedTicketException unrecognizedTicketException = Assertions.assertThrows(UnRecognizedTicketException.class, () -> parkingLotManager.fetchByParkingBoy(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedTicketException.getMessage());
    }

    @Test
    public void should_return_wrong_message_when_parking_car_by_parkingBoy_given_car_and_all_parkingBoy_with_parkingLot_without_position(){
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(new ParkingLot(1));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(new ParkingLot(1));
        ParkingLotServiceManager parkingLotManager = new ParkingLotServiceManager(smartParkingBoy);
        parkingLotManager.addParkingBoy(superSmartParkingBoy);

        Ticket ticket1 = parkingLotManager.parkByParkingBoy(car1);
        Ticket ticket2 = parkingLotManager.parkByParkingBoy(car2);


        UnAvailablePositionException unAvailablePositionException = Assertions.assertThrows(UnAvailablePositionException.class, () -> parkingLotManager.parkByParkingBoy(car3));
        Assertions.assertEquals("no available position", unAvailablePositionException.getMessage());
    }
}
