package com.parkinglot;

public class UnRecognizedTicketException extends RuntimeException {

    @Override
    public String getMessage() {
        return "Unrecognized parking ticket";
    }
}
