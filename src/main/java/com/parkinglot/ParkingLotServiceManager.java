package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ParkingLotServiceManager extends ParkingBoy {

    private List<ParkingBoy> parkingBoyList = new ArrayList<>();


    public ParkingLotServiceManager(ParkingLot parkingLot) {
        super(parkingLot);
    }

    public ParkingLotServiceManager(ParkingBoy parkingBoy) {
        this.parkingBoyList.add(parkingBoy);
    }

    public void addParkingBoy(ParkingBoy parkingBoy){
        this.parkingBoyList.add(parkingBoy);
    }


    public Ticket parkByParkingBoy(Car car) {
        List<ParkingBoy> availableParkingBoyList = getAvailableParkingBoy();
        if(availableParkingBoyList.isEmpty()) {
            throw new UnAvailablePositionException();
        }

        ParkingBoy parkingBoy = getParkingBoyByRandom(availableParkingBoyList);
        return parkingBoy.park(car);
    }

    public List<ParkingBoy> getAvailableParkingBoy(){
        List<ParkingBoy> availableParkingBoyList = new ArrayList<>();
        for (ParkingBoy parkingBoy : parkingBoyList) {
            if(parkingBoy.hasEmptyParkingLot()){
                availableParkingBoyList.add(parkingBoy);
            }
        }

        return availableParkingBoyList;
    }

    public ParkingBoy getParkingBoyByRandom(List<ParkingBoy> availableParkingBoy){
        if(availableParkingBoy.size() == 1) return availableParkingBoy.get(0);
        int parkingBoyIndex = new Random().nextInt(availableParkingBoy.size() - 1);
        return availableParkingBoy.get(parkingBoyIndex);
    }

    public Car fetchByParkingBoy(Ticket ticket) {
        for (ParkingBoy parkingBoy : parkingBoyList) {
            ParkingLot parkingLot = parkingBoy.containTicket(ticket);
            if(parkingLot != null) return parkingLot.fetch(ticket);
        }

        throw new UnRecognizedTicketException();
    }
}
