package com.parkinglot;

import java.util.ArrayList;
import java.util.List;

public class ParkingBoy {

    protected List<ParkingLot> parkingLotList = new ArrayList<>();

    public ParkingBoy(){}

    public ParkingBoy(ParkingLot parkingLot) {
        parkingLotList.add(parkingLot);
    }

    public Ticket park(Car car) {
        ParkingLot parkingLot = getCorrectParkingLot();
        if (parkingLot == null) {
            throw new UnAvailablePositionException();
        }
        return parkingLot.park(car);
    }

    public ParkingLot getCorrectParkingLot() {
        for (ParkingLot parkingLot : parkingLotList) {
            if (!parkingLot.isFull()) {
                return parkingLot;
            }
        }

        return null;
    }

    public void addParkingLot(ParkingLot parkingLot) {
        if (parkingLotList.contains(parkingLot)) return;
        parkingLotList.add(parkingLot);
    }

    public Car fetch(Ticket ticket) {

        ParkingLot parkingLot = containTicket(ticket);
        if(parkingLot != null) return parkingLot.fetch(ticket);

        throw new UnRecognizedTicketException();
    }

    public ParkingLot getParkingLot(int parkingLotNum) {
        return parkingLotList.get(parkingLotNum - 1);
    }

    public boolean hasEmptyParkingLot(){
        for (ParkingLot parkingLot : parkingLotList) {
            if(!parkingLot.isFull()) return true;
        }

        return false;
    }

    public ParkingLot containTicket(Ticket ticket){
        for (ParkingLot parkingLot : parkingLotList) {
            if (parkingLot.containCar(ticket)) {
                return parkingLot;
            }
        }

        return null;
    }

}
