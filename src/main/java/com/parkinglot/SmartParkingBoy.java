package com.parkinglot;

public class SmartParkingBoy extends ParkingBoy {

    public SmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }

    @Override
    public Ticket park(Car car) {

        ParkingLot parkingLot = getCorrectParkingLot();
        return parkingLot.park(car);
    }

    @Override
    public ParkingLot getCorrectParkingLot() {
        if (parkingLotList.isEmpty()) return null;

        ParkingLot moreEmptyParkingLot = parkingLotList.get(0);
        for (ParkingLot parkingLot : parkingLotList) {
            if (parkingLot.getEmptyPositionCount() > moreEmptyParkingLot.getEmptyPositionCount()) {
                moreEmptyParkingLot = parkingLot;
            }
        }

        return moreEmptyParkingLot;
    }

}
