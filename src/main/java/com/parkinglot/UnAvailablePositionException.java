package com.parkinglot;

public class UnAvailablePositionException extends RuntimeException {

    @Override
    public String getMessage() {
        return "no available position";
    }
}
