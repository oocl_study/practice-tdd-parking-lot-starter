package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private Integer positionCount = 10;

    private Map<Ticket, Car> ticketCarMap = new HashMap<>();


    public ParkingLot() {
    }

    public ParkingLot(Integer positionCount) {
        this.positionCount = positionCount;
    }

    public Ticket park(Car car) {
        if(ticketCarMap.size() >= positionCount){
            throw new UnAvailablePositionException();
        }

        Ticket ticket = new Ticket();
        ticketCarMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if(!ticketCarMap.containsKey(ticket)){
            throw new UnRecognizedTicketException();
        }
        return ticketCarMap.remove(ticket);
    }

    public boolean isFull(){
        return ticketCarMap.size() >= positionCount;
    }

    public boolean containCar(Ticket ticket){
        return ticketCarMap.containsKey(ticket);
    }

    public int getEmptyPositionCount(){
        return positionCount - ticketCarMap.size();
    }

    public double getParkingRate(){
        return 1.0 * getEmptyPositionCount() / positionCount;
    }
}
