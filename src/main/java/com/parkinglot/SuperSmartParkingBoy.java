package com.parkinglot;

public class SuperSmartParkingBoy extends ParkingBoy{

    public SuperSmartParkingBoy(ParkingLot parkingLot) {
        super(parkingLot);
    }

    @Override
    public Ticket park(Car car) {
        ParkingLot parkingLot = getCorrectParkingLot();
        return parkingLot.park(car);
    }

    @Override
    public ParkingLot getCorrectParkingLot(){
        if(parkingLotList.isEmpty()) return null;

        ParkingLot largerRateParkingLot = parkingLotList.get(0);
        for (ParkingLot parkingLot : parkingLotList) {
            if(parkingLot.getParkingRate() > largerRateParkingLot.getParkingRate()){
                largerRateParkingLot = parkingLot;
            }
        }

        return largerRateParkingLot;
    }

}
